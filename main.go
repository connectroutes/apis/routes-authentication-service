package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net"
	"os"
	"routes-authentication-service/proto"
)
import "google.golang.org/grpc"
import _ "github.com/joho/godotenv/autoload"


var db *mongo.Database


func main() {

	fmt.Println("starting")
	lis, err := net.Listen("tcp", "0.0.0.0:50052")

	if err!= nil{
		log.Fatalf("Could not listen on port: %v", err)
	}

	server := grpc.NewServer()

	proto.RegisterAuthenticationServiceServer(server, &service{})

	initServices()
	connectToDb()
	if err := server.Serve(lis); err != nil{
		log.Fatal(err)
	}
}

func connectToDb(){
	// connect to MongoDB
	client, err := mongo.NewClient(options.Client().ApplyURI(os.Getenv("MONGODB_DATABASE_URI")))
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil{
		panic(err)
	}

	fmt.Println("Connected to MongoDB")
	db = client.Database(os.Getenv("MONGODB_DATABASE_NAME"))
}
