package main

import (
	context2 "context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/nyaruka/phonenumbers"
	"gitlab.com/connectroutes/routes-go-lib/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"math/rand"
	"os"
	authModels "routes-authentication-service/models"
	"routes-authentication-service/proto"
	"routes-authentication-service/proto/external"
	"time"
)

type service struct{}

func (s *service) Login(context2.Context, *proto.LoginRequest) (*proto.LoginResponse, error) {
	panic("implement me")
}

func (s *service) ConfirmResetPassword(ctx context.Context, request *proto.ConfirmResetPasswordRequest) (*proto.ConfirmResetPasswordResponse, error) {
	passwordReset, err := authModels.GetPasswordResetToken(ctx, db, request.Token)

	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument,
			"Could not retrieve token")
	}

	//TODO revoke current sessions?
	err = models.UpdateUser(ctx, db, passwordReset.UserId, map[string]interface{}{
		"password": hashPassword(request.Password),
	})

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Could not update password")
	}

	return &proto.ConfirmResetPasswordResponse{
		Message: "Password reset successful",
	}, nil
}

func (s *service) ResetPassword(ctx context.Context, request *proto.ResetPasswordRequest) (*proto.ResetPasswordResponse, error) {

	user, err := models.GetUserFromEmailAddress(ctx, db, request.Email)

	message := "email reset link sent if an account is associated with email address"
	if err != nil {
		return &proto.ResetPasswordResponse{Message: message}, nil
	}

	token := uuid.New().String()

	//TODO prevent multiple requests with same email address

	_, err = authModels.CreatePasswordReset(ctx, db, bson.M{
		"email_address": user.EmailAddress,
		"token":         token,
		"user_id":       user.Id,
	})

	if err != nil {
		return &proto.ResetPasswordResponse{Message: message}, nil
	}

	_, err = external.NewNotificationServiceClient(notificationServiceConn).SendEmail(ctx, &external.SendEmailRequest{
		Recipient: user.EmailAddress,
		Message:   fmt.Sprintf("Please reset your password using this link <a href='https://app.routes.com.ng/reset_password/%s'>Reset your password</a>", token),
		Sender:    "Routes<password-resets@mail.routes.com.ng>",
		Subject:   "Reset your Routes password",
	})

	if err != nil {
		fmt.Println(err)
	}

	return &proto.ResetPasswordResponse{Message: message}, nil

}

func (s *service) SubmitPersonalInfo(ctx context.Context, req *proto.SubmitPersonalInfoRequest) (*proto.SubmitPersonalInfoResponse, error) {
	num, err := phonenumbers.Parse(req.PhoneNumber, "NG")

	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument,
			"Phone Number is Invalid")
	}

	formattedPhoneNumber := phonenumbers.Format(num, phonenumbers.INTERNATIONAL)

	isEmailUnique, err := models.IsUserEmailAddressUnique(ctx, db, req.EmailAddress)

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"System error: Could not ensure unique email address")
	}

	if !isEmailUnique {
		return nil, status.Errorf(codes.InvalidArgument,
			"There's an existing account with this email address. Please login instead or change your email address")
	}
	authenticated, err := models.VerifyOtpAuthenticationId(ctx, db, req.OtpAuthenticationId, formattedPhoneNumber)

	if err != nil {
		fmt.Println(err)
		return nil, status.Errorf(codes.Internal,
			"Could not verify authentication")
	}

	if !authenticated {
		return nil, status.Errorf(codes.InvalidArgument,
			"Could not verify authentication. Please request for new otp")
	}

	userId, err := models.CreateUser(ctx, db, bson.M{
		"name":          req.FullName,
		"phone_number":  formattedPhoneNumber,
		"email_address": req.EmailAddress,
		"password":      hashPassword(req.Password),
	})

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Could not create user")
	}

	_userId, _ := primitive.ObjectIDFromHex(userId)
	token, userData, err := createSession(&models.User{
		Id:           _userId,
		PhoneNumber:  formattedPhoneNumber,
		EmailAddress: req.EmailAddress,
		Name:         req.FullName,
	})

	if err != nil {
		fmt.Print(err)
		return nil, status.Errorf(codes.Internal,
			"Could not create token")
	}

	return &proto.SubmitPersonalInfoResponse{
		Token:    token,
		UserData: userData,
	}, nil
}

func (s *service) RequestOtp(ctx context.Context, req *proto.RequestOtpRequest) (*proto.RequestOtpResponse, error) {
	num, err := phonenumbers.Parse(req.PhoneNumber, "NG")

	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument,
			"Phone Number is Invalid")
	}

	formattedPhoneNumber := phonenumbers.Format(num, phonenumbers.INTERNATIONAL)

	doesPendingOtpExist, err := models.DoesPendingOtpRequestExist(ctx, db, formattedPhoneNumber)

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Could not confirm OTP creation")
	}

	if doesPendingOtpExist {
		return nil, status.Errorf(codes.InvalidArgument,
			fmt.Sprintf("Please try again after %s seconds", os.Getenv("OTP_TIMEOUT_SECONDS")))
	}

	otp := generateCode()

	_, err = models.CreateOtpRequest(ctx, db, bson.M{"otp": otp, "phone_number": formattedPhoneNumber})

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Could not create OTP")
	}

	_, err = external.NewNotificationServiceClient(notificationServiceConn).SendSms(ctx, &external.SendSmsRequest{
		PhoneNumber: phonenumbers.Format(num, phonenumbers.INTERNATIONAL),
		Message:     fmt.Sprintf("%d is your otp code for routes", otp),
	})

	if err != nil {
		fmt.Println("Send SMS error")
		fmt.Println(err)
	}

	return &proto.RequestOtpResponse{
		Message: "otp requested",
	}, nil

}

func (s *service) SubmitOtp(ctx context.Context, req *proto.SubmitOtpRequest) (*proto.SubmitOtpResponse, error) {
	num, err := phonenumbers.Parse(req.PhoneNumber, "NG")
	formattedPhoneNumber := phonenumbers.Format(num, phonenumbers.INTERNATIONAL)

	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument,
			"Phone Number is Invalid")
	}

	verificationId, err := models.VerifyOtp(ctx, db, formattedPhoneNumber, int(req.Otp))

	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Cannot verify otp")
	}

	if verificationId == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"Invalid Otp")
	}

	user, err := models.GetUserFromPhoneNumber(ctx, db, formattedPhoneNumber)

	if err != nil {
		// TODO check error
		//user not found

		return &proto.SubmitOtpResponse{
			OtpAuthenticationId: verificationId,
			IsNewUser:           true,
		}, nil
	} else {

		token, userData, err := createSession(user)

		if err != nil {
			return nil, status.Errorf(codes.Internal,
				"Could not create token")
		}

		return &proto.SubmitOtpResponse{
			Token:     token,
			IsNewUser: false,
			UserData:  userData,
		}, nil

	}

}

func generateCode() int {
	max := 9999
	min := 1000
	return rand.Intn(max-min) + min
}

func createSession(user *models.User) (string, string, error) {

	sessionId := user.Id.Hex() + ":" + uuid.New().String()
	user.UserId = user.Id.Hex()
	if user.DriverID != primitive.NilObjectID {
		// user is a driver
		driver, err := models.GetDriver(context.Background(), db, user.DriverID)
		if err != nil {
			fmt.Println(err)
		} else {
			user.Driver = *driver
		}
	}
	bytes, err := json.Marshal(user)

	if err != nil {
		return "", "", err
	}

	_, err = redisClient.HMSet(sessionId, map[string]interface{}{"user_id": user.Id.String(), "user_data": string(bytes)}).Result()

	if err != nil {
		return "", "", err
	}

	_, err = redisClient.Expire(sessionId, time.Hour*8760).Result() // a year

	return sessionId, string(bytes), nil
}
