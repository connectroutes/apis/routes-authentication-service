package models

import (
	"gitlab.com/connectroutes/routes-go-lib/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"time"
)

type PasswordReset struct {
	Id           primitive.ObjectID `bson:"_id"`
	EmailAddress string             `bson:"email_address" json:"email_address"`
	Token        string             `json:"token"`
	UserId       primitive.ObjectID `bson:"user_id" json:"user_id"`
	VerifiedAt   time.Time          `bson:"verified_at" json:"verified_at"`
	CreatedAt    time.Time          `bson:"createdAt" json:"created_at"`
	UpdatedAt    time.Time          `bson:"updatedAt" json:"updated_at"`
}

var collectionName = "password_resets"

func GetPasswordResetToken(ctx context.Context, db *mongo.Database, token string) (*PasswordReset, error) {
	var passwordRequest *PasswordReset

	err := db.Collection(collectionName).FindOne(ctx, bson.M{"token": token, "verified_at": nil}).Decode(passwordRequest)

	if err != nil {
		return nil, err
	}

	return passwordRequest, nil
}

func CreatePasswordReset(ctx context.Context, db *mongo.Database, document bson.M) (string, error) {
	return models.CreateDocument(ctx, db, collectionName, document)
}
