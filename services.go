package main

import (
	"fmt"
	"google.golang.org/grpc"
	"os"
)

var notificationServiceConn *grpc.ClientConn

func initServices() {
	var err error
	notificationServiceConn, err = grpc.Dial(os.Getenv("NOTIFICATION_SERVICE_URL"), grpc.WithInsecure())

	if err != nil {
		fmt.Println("cannot connect to notification service", err)
	}
}
